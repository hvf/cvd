# CVDrustkar
A flask website that creates CVs. Information can be added as necessary, and it is available in two languages.

# Requirements
Only python-docx 0.8.10 module is needed, which can be installed from pip:
```sh
pip install python-docx
```
